package sample.domain;

import java.util.*;

public class Organization {

    private UUID id;
    private String name;
    private List<Project> projects = new ArrayList<Project>();
    private Map<User, OrganizationRole> members = new HashMap<>();


    public Organization(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addProject(Project project) {
        projects.add(project);
    }

    public void addMember(OrganizationRole role, User user) {
        members.put(user, role);
    }

    public List<Project> getProjects() {
        return projects;
    }

    public String getName() {
        return name;
    }

    public OrganizationRole getUserRole(User user) {
        return members.get(user);
    }
}
