package sample.domain;

public class Context {


    private static User currentUser;

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        Context.currentUser = currentUser;
    }
}
