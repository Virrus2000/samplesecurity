package sample.services;


import sample.data.Repository;
import sample.domain.Context;
import sample.domain.Organization;
import sample.domain.OrganizationRole;
import sample.domain.User;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Service {
    private Repository repository = new Repository();

    public void registerUser(String name) {
        User user = new User(name);
        repository.add(user);
        Context.setCurrentUser(user);
    }

    public void createOrg(String name) throws Exception {
        if (Context.getCurrentUser() == null) {
            throw new Exception("security exception");
        }
        Organization organization = new Organization(UUID.randomUUID());
        organization.addMember(OrganizationRole.director, Context.getCurrentUser());
        repository.add(organization);
    }


    //пишем вот такой метод
    //use case: авторизованный пользователь может добавить в организацию другого пользователя по имени
    //а добавлять пользователей может только директор или HR
    //директор может добавлять кого угодно
    //HR иожет добавлять всех, кроме директора
    // 1 подход - влоб
    public void addUserToOrg(User user, OrganizationRole role, Organization org) throws Exception {
        if (Context.getCurrentUser() == null) {
            throw new Exception("security exception");
        }
        OrganizationRole userRole = org.getUserRole(Context.getCurrentUser());
        if (userRole == null) {
            throw new Exception("access denied, user not in org");
        }
        if (userRole == OrganizationRole.director) {
            org.addMember(role, user);
        } else if (userRole == OrganizationRole.hr) {
            if (role != OrganizationRole.director) {
                org.addMember(role, user);
            } else {
                throw new Exception("security exception, hr cant add director");
            }
        } else {
            throw new Exception("security exception, role in org not allowed");
        }


    }


    //похожий юзкейс, но для простоты поменяем его на
    //директор может добавлять пользователей, менеджеров и айчэров
    //HR может добавлять пользователей и менеджеров
    // и добавим гибкости (вдруг заказчик захочет позволить кому-нибудь что-нибудь)
    public void addUserToOrgExtended(User user, OrganizationRole roleForNewUser, Organization org) throws Exception {
        //по сути здесь выполняется запрос
        if (Context.getCurrentUser() == null) {
            throw new Exception("security exception");
        }
        OrganizationRole userRole = org.getUserRole(Context.getCurrentUser());
        if (userRole == null) {
            throw new Exception("access denied, user not in org");
        }
        if (!canAddUsersWithRole.containsKey(userRole)) {
            throw new Exception("security exception, role in org not allowed");
        }
        List<OrganizationRole> canAdd = canAddUsersWithRole.get(userRole);
        if (!canAdd.contains(roleForNewUser)){
           throw new Exception("security exception, " + userRole.name() + " cant add ");
        }
        // а здесь запрос вернул true
        org.addMember(roleForNewUser, user);
    }
    //это нужно понятное дело не в коде хранить да и правила можно по хитрее задавать, но пусть пока так
    private HashMap<OrganizationRole, List<OrganizationRole>> canAddUsersWithRole = new HashMap<OrganizationRole, List<OrganizationRole>>() {
        {
            put(OrganizationRole.director, Arrays.asList(OrganizationRole.hr, OrganizationRole.user, OrganizationRole.manager));//директор может добавлять hr-ов, просто пользователей(user) :) и менеджеров
            put(OrganizationRole.hr, Arrays.asList(OrganizationRole.manager, OrganizationRole.user));//hr может тоже добавлять менеджеров и юзеров
        }
    };

    //Но так ли это должно работать?
    //понятно, что когда мы ставим аннотацию над методом - перед входом в метод выполняется какой то код
    //поэтому для простоты я пишу псевдокод, который по идее должен быть спрятан через какой нибуд АОП за аннтоацией

    //лирическое отствпление
    //насколько я помню есть такой Spting Security AOP
    //и он нам дает писать такие штуки
    //@PreAuthorize("hasPremission('premission_str', #organization)");
    //public void someCall(Organization organization){}
    //
    //в основе лежит метод
    //acl.isGranted(Permission[] permission, Sid[] sids)
    //permission по умолчанию бывает read write admin и, т.к. работает через битовые маски, их объединения
    //естественно можно своих надобалять, но только 31 (но как всегда можно написать совсем свое)
    //sid (secure identity) - некоторый уникальный идентификатор, в нашем случае поодойдет имя пользователя (но может быть роль...)
    //acl - access control list, можно рассматривать как Map<Sid,Permission[]>
    //хранится это все в виде 4 х дополнительных табличек
    // если любопытно, вся эта каша описанна здесь http://docs.spring.io/spring-security/site/docs/3.0.x/reference/domain-acls.html
    // по сути этим всем мы позволяем сказать "давай ка к организации с id=X пользователь с id=Y будет иметь права =[A,B,C]"
    // единственное, что в таком подходе не понятно - как написать запрос для нашего addUserToOrgExtended

    //вот мне собственно и интересно, как в подходе, который ты озвучил в презентации, будет вгылядеть такой метод (addUserToOrgExtended)
    //или как по другому удовлетворить таким требованиям
    //это может быть вполне псевдокод, главное чтобы понятно было




    public void addProject(String projectName) {

    }

    public void editOrgName(UUID id, String name) {

    }

    public void editDocument(String id, String content) {

    }
}
